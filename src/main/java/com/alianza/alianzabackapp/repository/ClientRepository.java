package com.alianza.alianzabackapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alianza.alianzabackapp.entity.Client;

/**
 * Interface de gestion de datos para entidad Client
 * @author Usuario
 */
public interface ClientRepository extends JpaRepository<Client, Long>{
	
	List<Client> findBySharedKey(String sharedKey);

}
