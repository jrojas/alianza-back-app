package com.alianza.alianzabackapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlianzaBackAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlianzaBackAppApplication.class, args);
	}

}
