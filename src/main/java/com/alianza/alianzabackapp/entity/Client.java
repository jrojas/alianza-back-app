package com.alianza.alianzabackapp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import org.springframework.format.annotation.DateTimeFormat;

/**
 * Entity Clients
 * @author Usuario
 *
 */

@Entity
@Table(name = "client")
public class Client implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "Shared_Key")
	private String sharedKey;
	
	@Column(name = "Bussines_Id")
	private String bussinesId;	
	
	private String email;
	
	private String phone;
	
	@Column(name = "Date_Adde")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date dateAdde;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSharedKey() {
		return sharedKey;
	}

	public void setSharedKey(String sharedKey) {
		this.sharedKey = sharedKey;
	}

	public String getBussinesId() {
		return bussinesId;
	}

	public void setBussinesId(String bussinesId) {
		this.bussinesId = bussinesId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getDateAdde() {
		return dateAdde;
	}

	public void setDateAdde(Date dateAdde) {
		this.dateAdde = dateAdde;
	}

}
