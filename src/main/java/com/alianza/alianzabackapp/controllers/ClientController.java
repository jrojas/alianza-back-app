package com.alianza.alianzabackapp.controllers;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alianza.alianzabackapp.entity.Client;
import com.alianza.alianzabackapp.service.ClientService;
import com.alianza.alianzabackapp.service.ClientServiceImpl;

/**
 * Controlador de los servicios de Cliente
 * @author Usuario
 *
 */

@CrossOrigin(origins= {"http://localhost:4200"})
@RestController
@RequestMapping(value = "api/client")
public class ClientController {
	
	private static final Logger log = LoggerFactory.getLogger(ClientController.class);
	
	@Autowired
	ClientService clientService;
	
	/**
	 * Servicio que permite listar todos los clientes que existen
	 * @return
	 */
	@GetMapping("/list")
	public ResponseEntity<Object> listar(){
		return clientService.findALlClient();
	}
	
	/**
	 * Servicio que permite consultar los clients por sharedKey
	 * @return
	 */
	@GetMapping("/sharedKey/{sharedKey}")
	public ResponseEntity<Object> findBySharedKey(@PathVariable String sharedKey){
		return clientService.findBySharedKey(sharedKey);
	}
	
	/**
	 * Servicio que permite listar todos los clientes por Id
	 * @return
	 */
	@GetMapping("/{idClient}")
	public ResponseEntity<Object> findBySharedKey(@PathVariable Long idClient){
		return clientService.findCliendByID(idClient);
	}
	
	/**
	 * Servicio que permite crear un cliente
	 * @param client
	 * @return
	 */
	@PostMapping("/create")
	public ResponseEntity<Object> crear(@RequestBody Client client) {		
		return clientService.createClient(client);
		
	}
	
	/**
	 * Servicio que permite editar un cliente
	 * @param idClient
	 * @param client
	 * @return
	 */
	@PutMapping("/edit/{idClient}")
	public ResponseEntity<Object> crear(@PathVariable Long idClient,@RequestBody Client client) {		
		return clientService.editClient(idClient, client);
		
	}
	
	/**
	 * Servicio que permite generar un excel con los clientes
	 * @return
	 */
	@GetMapping("/export")
	public ResponseEntity<byte[]> getExcel() {
		try {
			byte[] excelContent = clientService.generateExcel();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", "data_client.xlsx");
			log.debug("Excel genetare succesfull");
			return new ResponseEntity<>(excelContent, headers, HttpStatus.OK);
		} catch (IOException e) {
			log.error("Error to generate Excel {}",e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
