package com.alianza.alianzabackapp.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.alianza.alianzabackapp.entity.Client;
import com.alianza.alianzabackapp.repository.ClientRepository;

@Service
public class ClientServiceImpl implements ClientService {

	private static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

	@Autowired
	private ClientRepository clientRepository;

	@Override
	public ResponseEntity<Object> createClient(Client client) {
		Client cl = clientRepository.save(client);
		log.info("Client {} register", client.getBussinesId());
		return new ResponseEntity<>(cl, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<Object> editClient(Long idClient, Client client) {
		Client cl = clientRepository.findById(idClient).orElse(null);
		if (cl == null) {
			return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
		}
		client.setId(idClient);
		log.info("Client {} edit", client.getBussinesId());
		cl = clientRepository.save(client);
		return new ResponseEntity<>(cl, HttpStatus.ACCEPTED);
	}

	@Override
	public ResponseEntity<Object> findALlClient() {
		List<Client> lsClient = clientRepository.findAll();
		return new ResponseEntity<>(lsClient, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findBySharedKey(String sharedKey) {
		List<Client> lsClient = clientRepository.findBySharedKey(sharedKey);
		return new ResponseEntity<>(lsClient, HttpStatus.OK);
	}

	@Override
	public byte[] generateExcel() throws IOException {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Data_Client");
		List<String> data = Arrays.asList("sharedKey", "bussinesId", "phone", "email", "dateAdde");
		Row headerRow = sheet.createRow(0);
		for (int i = 0; i < data.size(); i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(data.get(i));
		}
		List<Client> lsClient = clientRepository.findAll();
		int rowNum = 1;
		for (Client cl : lsClient) {
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(cl.getSharedKey());
			row.createCell(1).setCellValue(cl.getBussinesId());
			row.createCell(2).setCellValue(cl.getPhone());
			row.createCell(3).setCellValue(cl.getEmail());
			row.createCell(4).setCellValue(cl.getDateAdde().toString());
		}
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		workbook.write(outputStream);
		workbook.close();
		return outputStream.toByteArray();

	}

	@Override
	public ResponseEntity<Object> findCliendByID(Long idClient) {
		Client cl = clientRepository.findById(idClient).orElse(null);
		if (cl == null) {
			return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(cl, HttpStatus.OK);
	}

}
