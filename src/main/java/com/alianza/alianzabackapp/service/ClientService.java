package com.alianza.alianzabackapp.service;
import java.io.IOException;

import org.springframework.http.ResponseEntity;
import com.alianza.alianzabackapp.entity.Client;

public interface ClientService {	
	/**
	 * Metodo que permite persisir el cliente
	 * @param client
	 * @return
	 */
	public ResponseEntity<Object> createClient(Client client);
	/**
	 * Metodo que permite editar el cliente
	 * @param client
	 * @return
	 */
	public ResponseEntity<Object> editClient(Long idClient,Client client);
	
	/**
	 * Metodo que permite consultar los clientes por Id
	 * @param client
	 * @return
	 */
	public ResponseEntity<Object> findCliendByID(Long idClient);
	
	/**
	 * Metodo que permite consultar todos los clientes
	 * @param client
	 * @return
	 */
	public ResponseEntity<Object> findALlClient();
	
	/**
	 * Metodo que permite buscar un cleinte por sharedKey
	 * @param sharedKey
	 * @return
	 */
	public ResponseEntity<Object> findBySharedKey(String sharedKey);
	
	/**
	 * Metodo que permite generar el exel de la info de Clientes
	 * @return
	 * @throws IOException
	 */
	public byte[] generateExcel() throws IOException;

}
