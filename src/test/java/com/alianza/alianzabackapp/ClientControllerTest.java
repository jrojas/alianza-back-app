package com.alianza.alianzabackapp;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.alianza.alianzabackapp.entity.Client;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
public class ClientControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testGetAllUsers() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/client/list")).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(4));
	}

	@Test
	public void testSharedClient() throws Exception {
		String sharedKey = "jroa";
		mockMvc.perform(MockMvcRequestBuilders.get("/api/client/sharedKey/{sharedKey}", sharedKey))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
	}

	@Test
	public void testGenerateExcel() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/client/export"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void createClient() throws Exception {
		Client newClient = new Client();
		newClient.setBussinesId("juan roa");
		newClient.setEmail("jroa@gmail.com");
		newClient.setSharedKey("jroa");
		newClient.setPhone("31244444444");
		newClient.setDateAdde(new Date());
		mockMvc.perform(MockMvcRequestBuilders.post("/api/client/create").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(newClient)))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andExpect(MockMvcResultMatchers.jsonPath("$.email").value(newClient.getEmail()));;

		mockMvc.perform(MockMvcRequestBuilders.get("/api/client/list")).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(5)).andReturn();
	}

	@Test
	public void editClient() throws Exception {
		Client newClient = new Client();
		newClient.setBussinesId("juan roa");
		newClient.setEmail("jroa@gmail.com");
		newClient.setSharedKey("jroa");
		newClient.setPhone("31244444444");
		newClient.setDateAdde(new Date());
		long clienId = 3;
		mockMvc.perform(MockMvcRequestBuilders.put("/api/client/edit/{clienId}", clienId)
				.contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(newClient)))
				.andExpect(MockMvcResultMatchers.status().isAccepted()).andExpect(jsonPath("$.id").value(clienId))
				.andExpect(jsonPath("$.email").value("jroa@gmail.com"));

	}

}
